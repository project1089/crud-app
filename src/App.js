import Header from "./Components/Header/Header";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Footer from "./Components/Footer/Footer";
import Login from "./Components/Login/Login";
import Register from "./Components/Register/Register"
import Home from "./Components/Home/Home.";
import EmployeeList from "./Components/EmployeeList/EmployeeList";
import AddInformation from "./Components/AddInformation/AddInformation";
import EditInformation from "./Components/EditInformation/EditInformation";
import { Redirect } from "react-router";
function App() {
  return (
    <div>
      <Router>
          <Header />
          <Switch>
            <Route exact path = "/" component = {Home}/>

            <Route exact path="/list" component={() => localStorage.getItem('token') ?
              <EmployeeList /> : <Redirect to="/login" />} />

            <Route exact path="/add" component={() => localStorage.getItem('token') ?
              <AddInformation /> : <Redirect to="/login" />} />

            <Route exact path="/edit/:id" component={() => localStorage.getItem('token') ?
              <EditInformation /> : <Redirect to="/login" />} />

            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Register} />

          </Switch>


          <Footer />

        </Router>

    </div>

  );
}

export default App;
