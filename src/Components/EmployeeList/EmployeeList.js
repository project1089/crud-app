import React from 'react'
import { useState, useEffect } from 'react'
import Details from '../Details/Details'

export default function EmployeeList() {
    const [information, setinformation] = useState([])
    useEffect(() => {
        fetch("http://localhost:3004/details")
            .then(response => response.json())
            .then(data => {
                console.log(data)
                setinformation(data)
            })

    }, [])
    const DeleteInformationFunction = (id) => {
        //console.log(id);
        let filteredContacts = information.filter(x => x.id !== id)
        fetch(`http://localhost:3004/details/${id}`, {

            method: "DELETE"
        })

        setinformation(filteredContacts)
    }
    return (
        <div>
            <div className="container">
                <div className="row mt-5">
                    {
                        information.map(item => <Details DeleteContact={DeleteInformationFunction} key={item.id} id={item.id} fname={item.fname} lname={item.lname} age={item.age}
                            location={item.location} email={item.email} experience = {item.experience} />)
                    }

                </div>

            </div>
        </div>
    )
}
