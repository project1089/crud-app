import React from "react";
import { Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";
import office from "./office2.jpg"
import office1 from "./office1.jpg"


const Home = () => {
    return (
        <>
            <Carousel fade={true} pause={false}>
                <Carousel.Item interval={4000}>
                    <img
                        className="d-block w-100"
                        src={office}
                        alt="First slide"
                    height = "700vh"
                    />
                    {
                        (!localStorage.getItem('token'))?
                    <Carousel.Caption>
                        <button className="btn btn-dark h3">
                            <Link to="/login" className="text-light">LOGIN </Link> /
                            <Link to="/signup" className="text-light"> SIGNUP</Link>
                        </button>
                        <h3 className="text-light">Maintain the records</h3>
                    </Carousel.Caption>:
                    <Carousel.Caption>
                        <button className="btn btn-dark h1">
                            <Link to="/list" className="text-light">Browse List </Link> 
                        </button>
                        <h3 className="text-light">Maintain the records</h3>
                    </Carousel.Caption>
                   }
                </Carousel.Item>
                <Carousel.Item interval={4000}>
                    <img 
                        className="d-block w-100"
                        src={office1}
                        alt="Second slide"
                    height = "700vh"
                    />
                   {
                        (!localStorage.getItem('token'))?
                    <Carousel.Caption>
                        <button className="btn btn-dark h3">
                            <Link to="/login" className="text-light">LOGIN </Link> /
                            <Link to="/signup" className="text-light"> SIGNUP</Link>
                        </button>
                        <h3 className="text-light">Maintain the records</h3>
                    </Carousel.Caption>:
                    <Carousel.Caption>
                        <button className="btn btn-dark h1">
                            <Link to="/list" className="text-light">Browse List </Link> 
                        </button>
                        <h3 className="text-light">Maintain the records</h3>
                    </Carousel.Caption>
                   }
                </Carousel.Item>
            </Carousel>

        </>
    )
}

export default Home;