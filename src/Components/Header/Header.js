import React from 'react'
import { Link, useHistory } from 'react-router-dom'


export default function Header() {
    let history = useHistory()
    function logout(){
        localStorage.clear();
        history.push("/login")

    }
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-primary fixed-top">
                <div className="container-fluid">
                    <Link to="/" className="nav-link text-light badge bg-dark fs-3 fst-italic fw-bolder text-wrap">GL</Link>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">

                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0 text-center">

                            <li className="nav-item">
                                <Link to="/" className="nav-link text-light">Home</Link>
                            </li>
                            <li className="nav-item">

                                <Link to="/add" className="nav-link text-light">Add Information</Link>

                            </li>

                            <li className="nav-item">

                                <Link to="/list" className="nav-link text-light">Employee List</Link>

                            </li>
                           {
                               (!localStorage.getItem('token'))?
                            <li className="nav-item">

                                <Link to="/login" className="nav-link text-light">Login</Link>

                            </li>:null
                           }
                           {
                               (!localStorage.getItem('token'))?
                            <li className="nav-item">

                                <Link to="/signup" className="nav-link text-light">Register</Link>

                            </li>:
                            <button className='btn btn-outline-light text-light' onClick={logout}>LogOut</button>
                           }
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    )
}
