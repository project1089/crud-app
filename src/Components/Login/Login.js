import React from 'react'
import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Link } from 'react-router-dom'



export default function Login() {
    const history = useHistory("")
    const [username, setusername] = useState()
    const [password, setpassword] = useState()
    let count = 0



    const loginHandler = () => {
        fetch("http://localhost:3004/userDetails")
            .then(response => response.json())
            .then(data => {
                //console.log(data)
                let authSuccess = 0;
                data.map((user) => {
                    //  console.log("in map")
                    if (user.email === username && user.password === password) {
                        authSuccess = 1;
                        localStorage.setItem('token', username)
                        console.log("authenticated")
                    }

                })
                if (authSuccess === 1) {
                    // console.log("login successful")
                    document.getElementById("mtch").innerHTML = "";
                    history.push("/list")
                    window.location.reload();
                } else {
                    document.getElementById("mtch").innerHTML = "The given credentials do not match!";
                }
            })

    }



    return (

        <div className="container ">

            <div className="row mt-5">
                <h1 className="primary col-md-6 mt-3 h1 fw-bolder" id="loginheading">
                    LogIn Here<br />
                </h1>
                <div className="col-md-6 mt-5">
                    {/* <label className="text-start">UserName or Email</label> */}


                    <label className="form-label h5" for="form3Example3">Email address</label>
                    <input type="email" onChange={(e) => setusername(e.target.value)}
                        id="form3Example3" className="form-control form-control-lg"
                        placeholder="Enter a valid email address" required />



                    {/* <label className="float-left">Password</label><label className="text-end"> <Link to="/forgot" className="text-dark float-right">Forgot Password?</Link></label> */}
                    <label className="form-label text-start h5" for="form3Example4">Password</label>
                    <input type="password" onChange={(e) => setpassword(e.target.value)} id="form3Example4" className="form-control form-control-lg"
                        placeholder="Enter password" required />

                    <p className="form-errors text-danger" id="mtch"></p>

                    <div className="mb-2">
                        <button id = "btnLogin" className="btn btn-primary col-12" onClick={loginHandler}>LogIn</button>
                    </div>
                    <div className="mb-2">
                        <button className="btn btn-outline-primary col-12 text-dark" >
                            <Link to="/signup" className="text-dark">Create New Account </Link> 
                        </button>
                        
                    </div>
                </div>
            </div>

        </div>

    )
}

