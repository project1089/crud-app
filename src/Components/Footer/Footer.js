import React, { Component } from 'react'
import './Footer.css';

export default class Footer extends Component {
    render() {
        return (
            <div className = "footer ">
                <div className="badge bg-dark fs-4 fst-italic fw-bolder text-wrap mt-2 mb-2">
                    GL   
                </div>
                &copy; 2021
            </div>
        )
    }
}
