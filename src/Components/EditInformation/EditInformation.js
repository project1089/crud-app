import React from 'react'
import { useParams } from 'react-router-dom'
import { useState,useEffect } from 'react'
import { useHistory } from 'react-router'

export default function EditInformation() {
    let {id}=useParams()
    const [fname, setfname] = useState("")
    const [lname, setlname] = useState("")
    const [email, setemail] = useState("")
    const [age, setage] = useState("")
    const [location, setlocation] = useState("")
    const [experience, setexperience] = useState("")

    useEffect(() => {
        fetch(`http://localhost:3004/details/${id}`)
        .then(response=>response.json())
        .then(data=>{
            console.log(data);
            setfname(data.fname)
            setlname(data.lname)
            setemail(data.email)
            setage(data.age)
            setlocation(data.location)
            setexperience(data.experience)
        })
        
    }, [])

    const history=useHistory()
    const editInformation=()=>{
        //    console.log(contact)
        fetch(`http://localhost:3004/details/${id}`,{
            method:"PUT",
            headers:{
                "Content-type":"application/json"
            },
            body:JSON.stringify({fname,lname,email,age,location,experience})
        })
        history.push("/")  
    }
    return (
        <div>
            <h1>Edit Information</h1>
            <div className="container">
            <div className="row mt-3">
            <div className="col-md-4 offset-md-4">
            <div className="mt-2">
            <input type="text" value={fname} className="form-control" onChange={(e)=>setfname(e.target.value)} placeholder="First Name"/>
            </div>
            <div className="mt-2">
            <input type="text"  value={lname} className="form-control" onChange={(e)=>setlname(e.target.value)} placeholder="Last Name"/>
            </div>
            <div className="mt-2">
            <input type="email"  value={email} className="form-control" onChange={(e)=>setemail(e.target.value)} placeholder="email"/>
            </div>
            <div className="mt-2">
            <input type="number"  value={age} className="form-control" onChange={(e)=>setage(e.target.value)} placeholder="age"/>
            </div>
            <div className="mt-2">
            <input type="text"  value={location} className="form-control" onChange={(e)=>setlocation(e.target.value)} placeholder="location"/>
            </div>
            <div className="mt-2">
            <input type="text"  value={experience} className="form-control" onChange={(e)=>setlocation(e.target.value)} placeholder="Work Experience"/>
            </div>
            <div className="mt-2">
            <button data-testid="btnSubmit" className="btn btn-primary col-12"  onClick={editInformation}>Edit Information</button>
            </div>
            </div>
            </div>
        </div>
           
        </div>
    )
}
