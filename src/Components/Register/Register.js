import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'





export default function Register() {
    const { register, handleSubmit, formState: { errors } } = useForm()
    const history = useHistory("")
    let alertMessage = null;
    let idAlreadyExists = false;
    const createAccount = (data) => {
        console.log(data)
        // console.log(data.contact, data.fullname)
        let name = data.fullname;
        let contact = data.contact
        let password = data.password;
        let email = data.email;
        let confirmpass = data.confirmpass;
        let favourites = new Array();


        if (confirmpass != password) {
            document.getElementById("msg").innerHTML = "Password does not match";

        }
        else {
            document.getElementById("msg").innerHTML = "";
            fetch("http://localhost:3004/userDetails")
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    data.map((user) => {
                        if (user.email === email) {
                            idAlreadyExists = true
                            //console.log("email matched")
                        }
                    })
                    if (idAlreadyExists) {
                        //console.log("This email id is already taken")
                        document.getElementById("exists").innerHTML = "This email id is already taken";
                        history.push("/signup");
                    }


                    else {
                        document.getElementById("msg").innerHTML = "";
                        console.log("accout created successfully")
                        fetch('http://localhost:3004/userDetails', {
                            method: "POST",
                            headers: {
                                "Content-type": "application/json"
                            },
                            body: JSON.stringify({ email, name, contact, password, favourites }),
                        })
                        history.push("/login");
                    }
 
                })

        }
    }

    return (

        <div className="container">
            <div className="row mt-5">

                <h3 className="fw-bolder col-md-4 mt-5 h1" id="signupheading">
                    Signup for free<br />
                </h3>


                <div className="col-md-6 offset-md-2">

                    <form onSubmit={handleSubmit(createAccount)}>


                        <div className="form-outline mb-4 mt-5">
                            <label className="form-label h5" for="form3Example3">Name</label>
                            <input type="text" id="form3Example3" className="form-control form-control-lg name"
                                placeholder="Enter your name" {...register("fullname", {
                                    required: true,
                                    maxLength: 20,
                                    minLength: 4
                                })} />
                            <p className="form-errors text-danger">{errors.fullname?.type === 'required'
                                && "Input length should be more than 3 and less than 20"}</p>

                        </div>


                        <div className="form-outline mb-4 ">
                            <label className="form-label h5" for="form3Example3">Email address</label>
                            <input type="email" id="form3Example3" className="form-control form-control-lg email"
                                placeholder="Enter a valid email address"  {...register("email", {
                                    required: true,
                                    pattern: {
                                        value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                    },
                                })} />
                            <p className="form-errors text-danger" id="email-error">{(errors.email?.type
                                === 'required' || errors.email?.type === 'pattern')
                                && "Enter valid email"}</p>
                            <p className="form-errors text-danger" id="exists"></p>

                        </div>


                        <div className="form-outline mb-4 ">
                            <label className="form-label h5" for="form3Example3">Contact number</label>
                            <input type="number" id="form3Example3" className="form-control form-control-lg contact"
                                placeholder="Enter a valid phone number" {...register("contact", {
                                    required: true,
                                    pattern: {
                                        value: /(7|8|9)\d{9}/
                                    },
                                    maxLength: 10
                                })} />
                            <p className="form-errors text-danger" id="contact-error">{(errors.contact?.type
                                === 'required'
                                || errors.contact?.type === 'pattern'
                                || errors.contact?.type === 'maxLength')
                                && "Enter valid 10 digit number"}</p>
                        </div>


                        <div className="form-outline mb-3">
                                    <label className="form-label h5" for="form3Example4">Password</label>
                                    <input type="password" id="form3Example4" className="form-control form-control-lg password"
                                        placeholder="Enter password" placeholder="Password"
                                        {...register("password", {
                                            required: true,
                                            pattern: {
                                                value: /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{5,}$/
                                            },
                                            minLength: 6,
                                        })} />
                                    <p className="form-errors text-danger" id ="password-error">{(errors.password?.type === 'required'
                                        || errors.password?.type === 'minLength'
                                        || errors.password?.type === 'pattern')
                                         && "Password should contain a upper and lower case letter, a speacial character, a digit and the length should be more than 5"}</p>

                                </div>

                                <div className="form-outline mb-3">
                                    <label className="form-label h5" for="form3Example4">Confirm Password</label>
                                    <input type="password" id="form3Example4" className="form-control form-control-lg cpassword"
                                        placeholder="Confirm password" {...register("confirmpass", {
                                            required: true,
                                            minLength: 6,
                                        })}
                                    />
                                    <p className="form-errors text-danger" id="msg"></p>

                                </div>


                                <div className="text-center col-lg-12 mt-4 pt-2">
                                    <button type="submit" className="btn btn-primary btn-lg loginbtn" id="btnSignup"
                                    >Register</button>
                                    <p className="small fw-bold mt-2 pt-1 mb-0">Already have an account?<Link
                                        to="/login" className="link-primary">
                                        Login
                                    </Link>
                                    </p>
                                </div>

                    </form>

                </div>
            </div>

        </div>

    )
}

