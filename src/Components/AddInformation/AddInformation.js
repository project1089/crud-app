import React from 'react'
import { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';

export default function AddInformation() {
    const [fname, setfname] = useState("")
    const [lname, setlname] = useState("")
    const [age, setage] = useState("")
    const [email, setemail] = useState("")
    const [location, setlocation] = useState("")
    const history=useHistory()
    const addInformationHandeller=()=>{
        //    console.log(contact)
        fetch("http://localhost:3004/details",{
            method:"POST",
            headers:{
                "Content-type":"application/json"
            },
            body:JSON.stringify({id:uuidv4(),fname,lname,email,age,location})
        })
        history.push("/")  
    }


    return (
        <div className="container">
            <div  className = "row mt-5">
                <div className="col-md-4 offset-md-4 mt-5">
                    <div className = "mt-2">
                        <input type="text" placeholder="Enter First Name" className="form-control" onChange={(e)=>setfname(e.target.value)}/>
                    </div>
                    <div className = "mt-2">
                        <input type="text" placeholder="Enter Lats Name" className="form-control" onChange={(e)=>setlname(e.target.value)}/>
                    </div>
                    <div className = "mt-2">
                        <input type="text" placeholder="Enter Email Id" className="form-control" onChange={(e)=>setemail(e.target.value)}/>
                    </div>
                    <div className = "mt-2">
                        <input type="text" placeholder="Enter Location" className="form-control" onChange={(e)=>setlocation(e.target.value)}/>
                    </div>
                    <div className = "mt-2">
                        <input type="number" placeholder="Enter Age" className="form-control" onChange={(e)=>setage(e.target.value)}/>
                    </div>
                    <div className = "mt-2">
                        <input type="number" placeholder="Enter Work Experience" className="form-control" onChange={(e)=>setage(e.target.value)}/>
                    </div>
                    <div className="mt-2">
                    <button className = "btn btn-outline-primary center col-12" onClick={addInformationHandeller}>Add Information</button> 
                    </div>
                </div>
            </div>
            
        </div>
    )
}
