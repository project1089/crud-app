import React from 'react'
import { Link } from 'react-router-dom'


export default function Details(props) {
    const DeleteInformation = (id) => {
        // console.log(id);
        props.DeleteContact(id)
    }
    return (
        <div className="col-md-4 mt-5">
            <div className="card mt-2">
                <div className ="card-body">
                <Link to={`/edit/${props.id}`}><i className="fa-solid fa-user-pen float-end fa-1x"></i></Link>
                <i className="fas fa-ban float-end fa-1x" onClick = {DeleteInformation.bind(this,props.id)}></i>
                <h5 className ="card-title">Name : {props.fname} {props.lname}</h5>
                <p className ="card-text">Email : {props.email}</p>
                <p className ="card-text">Location : {props.location}</p>
                <p className ="card-text">Age : {props.age}</p>
                <p className ="card-text">Work Experience : {props.experience}</p>

                </div>
            </div>
        </div>
    )
}
